from django.shortcuts import get_object_or_404, render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from .models import BookList, BookGenre, Book

# Für API
import json
import requests


def index(request):
    listen = BookList.objects.all()
    return render(request, 'mybooksapp/index.html', {'listen':listen})

def detail_list(request, booklist_id):
    liste = get_object_or_404(BookList, pk=booklist_id)
    books = Book.objects.filter(booklist=booklist_id)
    return render(request, 'mybooksapp/detail_list.html', {'liste':liste, 'books':books})

def detail_book(request, book_id):
    book = get_object_or_404(Book, pk=book_id)
    return render(request, 'mybooksapp/detail_book.html', {'book':book})

def new_book(request):
    if "isbn_search" in request.POST:
        if request.POST["isbn_search"] != "":
            user_agent = 'JSONImporter/0.1 (https://beep.boop.com/; notgonna@say.it); Python/3.8.5'
            headers = {'User-Agent': user_agent, 'Accept': 'text/json'}

            isbn_str = request.POST["isbn_search"]
            # Nicht-numerische Zeichen entfernen
            isbn_filter = filter(str.isdigit, isbn_str)
            isbn_str = "".join(isbn_filter)

            url = "https://www.googleapis.com/books/v1/volumes?q=isbn:" + isbn_str

            response = requests.get(url,headers=headers)
            source = response.content
            data_dict = json.loads(source)

            isbn_number = data_dict["items"][0]["volumeInfo"]["industryIdentifiers"][0]["identifier"]
            isbn_title = data_dict["items"][0]["volumeInfo"]["title"]
            isbn_author = data_dict["items"][0]["volumeInfo"]["authors"][0]  # Unzureichend, wenn mehr als ein*e Autor*in
            isbn_description = data_dict["items"][0]["volumeInfo"]["description"]
            isbn_cover = data_dict["items"][0]["volumeInfo"]["imageLinks"]["smallThumbnail"]
            isbn_context = {"isbn_number": isbn_number, "isbn_title": isbn_title, "isbn_author": isbn_author, "isbn_description": isbn_description, "isbn_cover": isbn_cover, "booklists": BookList.objects.all(), "genres": BookGenre.objects.all()}
            
            print(isbn_context)

            return render(request, 'mybooksapp/new_book.html', isbn_context)

    if request.method == 'POST':
        print(request.POST)
        isbn = request.POST["isbn"]
        title = request.POST["title"]
        author = request.POST["author"]
        content = request.POST["content"]
        rating = request.POST["rating"]
        price = request.POST["price"]
        cover = request.POST["cover"]
        booklist = BookList.objects.get(pk=request.POST["booklist"])
        genre = BookGenre.objects.get(pk=request.POST["genre"])
        Book.objects.create(title=title,
                            author=author,
                            content=content,
                            rating=rating,
                            price=price,
                            cover=cover,
                            booklist=booklist,
                            genre=genre,
                            isbn=isbn)
        print(request.POST)
        return HttpResponseRedirect(reverse('mybooksapp:detail_list', args=[booklist.id]))

    return render(request, 'mybooksapp/new_book.html', {"booklists": BookList.objects.all(), "genres": BookGenre.objects.all()})


def delete_book(request, book_id):
    book = get_object_or_404(Book, pk=book_id)
    book.delete()
    return HttpResponseRedirect(reverse('mybooksapp:index'))

def update_book_def(book, post):
    if "book_title" in post:
        book.title = post["book_title"]
    if "book_author" in post:
        book.author = post["book_author"]
    if "book_content" in post:
        book.content = post["book_content"]    
    if "book_rating" in post:
        try:
            rating = int(post["book_rating"]) 
        except ValueError:
            rating = 0
        book.rating = rating
    book.done = "book_done" in post
    if "book_price" in post:
        try:
            price = float(post["book_price"]) 
        except ValueError:
            price = 0
        book.price = price
    if "book_cover" in post:
        book.cover = post["book_cover"]
    if "book_booklist" in post:
        book.booklist = get_object_or_404(BookList, pk=post["book_booklist"])
    if "book_genre" in post:
        book.genre = get_object_or_404(BookGenre, pk=post["book_genre"])
    book.save()


def update_book(request, book_id):
    book = get_object_or_404(Book, pk=book_id)
    if request.POST:
        update_book_def(book, request.POST)
        return HttpResponseRedirect(reverse('mybooksapp:detail_book', args=[book.id]))
    listen = BookList.objects.all()
    genres = BookGenre.objects.all()
    context = {
            "listen": listen, 
            "genres": genres, 
            "book": book, 
            }
    return render(request, "mybooksapp/update_book_def.html", context)
